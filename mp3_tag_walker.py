#! /usr/bin/env python
# coding: utf-8

from __future__ import print_function
from clingon import clingon
from os.path import splitext, basename, dirname
import re
from six import add_metaclass
import sys

if sys.version[0] == '2':
    from scandir import scandir
    input = raw_input
    def items(d):
        return d.iteritems()
    def keys(d):
        return d.keys()
else:
    from os import scandir
    def items(d):
        return d.items()
    def keys(d):
        return list(d.keys())


date_pat = re.compile(r"(.+?)\((\d+)\)")


class IterateFiles:
    """ Iterate files in a directory with controlled recursion depth, optional forbidden subdirs
    """
    def __init__(self, directory, except_dirs=(), depth=1):
        self.directory, self.except_dirs, self.depth = directory, except_dirs, depth
        self.allowed_extensions = ['.mp3', '.MP3']

    def __iter__(self):
        return self.iterate_files(self.directory, self.depth)

    def iterate_files(self, directory, depth):
        for entry in scandir(directory):
            if entry.is_file() and \
               (not self.allowed_extensions or splitext(entry.name)[-1] in self.allowed_extensions):
                yield entry
            elif entry.is_dir() and depth > 1 and entry.name not in self.except_dirs:
                # will upgrade to 'yield from' soon
                for x in self.iterate_files(entry.path, depth-1):
                    yield x


def add_date(data, field, field_name):
    match = date_pat.search(field)
    if match:
        field, date = match.groups()
        data['date'] = date
        data[field_name] = field.strip()
    return data


def extract_fields(file):
    special_dirs = ('Albums', 'Compils', 'Stages')
    name = splitext(file.name)[0]
    fields = name.split(' - ')
    last_dir = basename(dirname(file.path))
    if last_dir in special_dirs:
        if len(fields) == 1:
            return add_date({'album': fields[0]}, fields[0], 'album')
        if len(fields) == 2:
            return add_date({'artist': fields[0], 'album': fields[1]}, fields[1], 'album')
    else:
        if len(fields) == 1:
            return add_date({'title': fields[0]}, fields[0], 'title')
        if len(fields) == 2:
            return add_date({'artist': fields[0], 'title': fields[1]}, fields[1], 'title')
        if len(fields) == 3:
            return add_date({'artist': fields[0], 'album': fields[1], 'title': fields[2]}, fields[1], 'album')
    print("Unmanaged file: {}".format(name))
    return {}


class FileTagsMeta(type):
    def __new__(mcs, name, bases, dico):
        cls = super(FileTagsMeta, mcs).__new__(mcs, name, bases, dico)
        if bases:
            if 'name' not in dico:
                raise RuntimeError('missing name')
            if dico['name'] in cls._processors:
                raise RuntimeError('duplicate name')
            cls._processors[dico['name']] = cls
            cls.processor = None
        return cls


class FileTagsBase(object):
    _processors = {}

    @classmethod
    def get_cls(cls, processor):
        return cls._processors[processor]

    @classmethod
    def keys(cls):
        return keys(cls._processors)

    @classmethod
    def load_processor(cls):
        if not cls.processor:
            cls.processor = cls.get_processor()
        return cls.processor

    def __init__(self, file):
        self.file = file
        self.file_tags = self.load_processor()(file)


@add_metaclass(FileTagsMeta)
class MutagenTags(FileTagsBase):
    name = 'mutagen'

    @classmethod
    def get_processor(cls):
        from mutagen.mp3 import EasyMP3
        return EasyMP3

    def get(self, item):
        try:
            return self.file_tags[item][0].strip()
        except KeyError:
            pass

    def set(self, item, value):
        self.file_tags[item] = value

    def save(self):
        self.file_tags.save()


@add_metaclass(FileTagsMeta)
class Eyed3Tags(FileTagsBase):
    name = 'eyed3'

    @classmethod
    def get_processor(cls):
        return None


class FileTags:
    tags = ('artist', 'album', 'title', 'date')

    def __init__(self, file, processor):
        self.file = file
        if issubclass(processor, FileTagsBase):
            self.processor = processor(file)
        else:
            self.processor = FileTagsBase.get_cls(processor)(file)

    def get(self, item):
        if item not in self.tags:
            raise RuntimeError('tag error: {}'.format(item))
        return self.processor.get(item)

    def set(self, item, value):
        self.processor.set(item, value)

    def save(self):
        self.processor.save()


def update_tags(processor, file, fields, do=True):
    tags = FileTags(file.path, processor)
    change = {}
    for k, v in items(fields):
        tag = tags.get(k)
        if tag:
            if tag != v.strip():
                if do:
                    print("change {} {}:{}->{}".format(file.path, k, tag, v))
                change[k] = v
        else:
            if do:
                print("set {} {}:{}".format(file.path, k, v))
            change[k] = v
    if change and do and input("Apply changes ? ").lower()[:1] == 'y':
        try:
            for k, v in items(change):
                tags.set(k, v)
            tags.save()
            print("Change done")
        except Exception:
            print("Change failed")
    return change


@clingon.clize
def script(folder, depth=1, lib='mutagen', show=False, version=False):
    """
    Set mp3 tags according to file name
    Cases:
    1- title (date)
    2- artist - title (date)
    3- artist - album - title (date)
    4- {Albums|Compils|Stages}/album (date)
    5- {Albums|Compils|Stages}/artist - album (date)
    """
    if version:
        print("Python {} from {}".format(sys.version, sys.executable))
        return
    try:
        processor = FileTagsBase.get_cls(lib)
    except KeyError:
        print("Unknown ID3 processor: {}, use one of {}".format(lib, FileTagsBase.keys()))
        return 1
    print("Processing {} (depth {}) with {}".format(folder, depth, lib))
    try:
        for file in IterateFiles(folder, depth=depth):
            fields = extract_fields(file)
            if show:
                fields = update_tags(processor, file, fields, do=False)
                print("File {}\n  --> {}".format(file.path, fields))
            elif update_tags(processor, file, fields):
                print()
    except KeyboardInterrupt:
        print('\nAbort')
        return 2
