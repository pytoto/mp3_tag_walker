# mp3_tag_walker

A simple python script to automatically tag mp3 files according to my file naming convention

Cases:

    1- title (date)
    2- artist - title (date)
    3- artist - album - title (date)
    4- {Albums|Compils|Stages}/album (date)
    5- {Albums|Compils|Stages}/artist - album (date)

Depends on [mp3-tagger](https://pypi.org/project/mp3-tagger/)
